#ifndef _BMP_H
#define _BMP_H
#include "../image/image.h"
#include <stdint.h>

#pragma pack(push, 1)

struct bmp_header
{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};

#pragma pack(pop)

enum bmp_constants {
    BF_TYPE_VALUE = 0x4D42,
    BI_DEFAULT_SIZE = 40,
    BI_DEFAULT_BIT_COUNT = 24,
    BI_DEFAULT_COMPRESSION = 0,
    BI_DEFAULT_RESOLUTION = 2834,
    BI_DEFAULT_CLR_USED = 0,
    BI_DEFAULT_CLR_IMPORTANT = 0
};

enum read_status  {
  READ_OK = 0,
  READ_ERROR,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER,
  READ_INVALID_MEMORY
  };

enum  write_status  {
  WRITE_OK = 0,
  WRITE_ERROR
};

enum read_status from_bmp(FILE* in, struct image* image );

enum write_status to_bmp(FILE* out, struct image const* image);

#endif

