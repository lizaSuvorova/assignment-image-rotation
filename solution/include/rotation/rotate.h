#ifndef _ROTATE_H
#define _ROTATE_H

#include "../image/image.h"

struct image rotate(struct image* img , struct image source );

#endif
