#ifndef _IMAGE_H
#define _IMAGE_H

#include <inttypes.h>
#include <stdio.h>

struct image {
  uint64_t width, height;
  struct pixel* data;
};

struct pixel {
  uint8_t b , g , r; 
};

uint64_t calculate_padding(uint64_t width);
struct image create(struct image* image , uint64_t width, uint64_t height);
void free_memory(struct image* image);

#endif





