#include "../include/bmp/bmp.h"
#include "../include/rotation/rotate.h"
#include <stdio.h>
#include <stdlib.h>
#define ARG_LEN 3
#define ERR_CODE 1

int main(int argc, char** argv ) {
    if(argc != ARG_LEN){
        printf("Invalid arguments count");
        return ERR_CODE;
    }

    FILE* file = fopen(argv[1] , "rb");

    if(!file){
        printf("Wrong first file");
        return ERR_CODE;
    }

    struct image* image;
    image = (struct image*) malloc(sizeof(struct image));

    if(image == NULL) {
        fclose(file);
        return ERR_CODE;
    }

    if(from_bmp(file , image)) {
        free_memory(image);
        printf("Error converting from bmp");
        fclose(file);
        return ERR_CODE;
    }
    fclose(file);

    struct image* rotate_image;
    rotate_image = (struct image*) malloc(sizeof(struct image));
    *rotate_image = rotate(rotate_image , *image);

    if(rotate_image == NULL){
        free_memory(image);
        fclose(file);
        return ERR_CODE;
    }

    free_memory(image);

    if(rotate_image->data == NULL) {
        free_memory(rotate_image);
        printf("Memory allocation error");
        return ERR_CODE;
    }

    FILE* file2 = fopen(argv[2] , "wb");

    if(!file2){
        printf("Wrong second file");
        return ERR_CODE;
    }

    if(to_bmp(file2 , rotate_image)) {
        free_memory(rotate_image);
        printf("Error converting to bmp");
        fclose(file2);
        return ERR_CODE;
    }

    fclose(file2);
    free_memory(rotate_image);
    return 0;
}
