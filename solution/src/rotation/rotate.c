#include "../../include/rotation/rotate.h"
#include "../../include/image/image.h"
#include <malloc.h>

struct image rotate(struct image* img , struct image const source) {
    *img = create(img, source.height, source.width);
    if (!img->data) return (struct image) {.width = source.height, .height = source.width, .data = NULL};

    for (uint64_t i = 0; i < source.width; i++) {
        for (uint64_t j = 0; j < source.height; j++) {
            img->data[i * source.height + j] = source.data[(source.height - j - 1) * source.width + i];
        }
    }
    return *img;
}
