#include "../../include/bmp/bmp.h"

enum read_status from_bmp(FILE* in, struct image* image){
    struct bmp_header bmp;
    if (image == NULL || in == NULL) {
        return READ_ERROR;
    }

    if (fread(&bmp, sizeof(struct bmp_header), 1, in) != 1){
        return READ_INVALID_HEADER;
    }

    *image = create(image , bmp.biWidth , bmp.biHeight);

    if(!image->data){
        return READ_INVALID_MEMORY;
    }

    uint8_t padding = calculate_padding(image->width);

    struct pixel* pixels = image->data;
    for(size_t i = 0; i < image->height; i++){
        size_t read_row = fread(pixels + image->width * i , sizeof(struct pixel) , image->width , in);
        if(read_row != image->width) return READ_INVALID_BITS;
        if(fseek(in , padding , SEEK_CUR) != 0) return READ_INVALID_SIGNATURE;
    }
    return READ_OK;

}

enum write_status to_bmp(FILE* out, struct image const* image) {
    size_t image_size = image->height * image->width * sizeof(struct pixel);
    struct bmp_header bmp_h = {
            .bfType = BF_TYPE_VALUE,
            .bfileSize  = sizeof(struct bmp_header) + image_size,
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = BI_DEFAULT_SIZE,
            .biWidth = image->width,
            .biHeight = image->height,
            .biPlanes = 1,
            .biBitCount = BI_DEFAULT_BIT_COUNT,
            .biCompression = BI_DEFAULT_COMPRESSION,
            .biSizeImage = image_size,
            .biXPelsPerMeter = BI_DEFAULT_RESOLUTION,
            .biYPelsPerMeter = BI_DEFAULT_RESOLUTION,
            .biClrUsed = BI_DEFAULT_CLR_USED,
            .biClrImportant = BI_DEFAULT_CLR_IMPORTANT
    };

    if (fwrite(&bmp_h, sizeof(struct bmp_header), 1, out) != 1) {
        return WRITE_ERROR;
    }

    uint8_t padding = calculate_padding(image->width);

    uint8_t empty_pixel[3] = {0};
    for (size_t i = 0; i < image->height; i++) {
        size_t write_row = fwrite(image->data + i * image->width, sizeof(struct pixel) * image->width, 1, out);
        if (write_row != 1) return WRITE_ERROR;
        if (padding != 0) {
            if (fwrite(empty_pixel, padding, 1, out) != 1) return WRITE_ERROR;
        }
    }
    return WRITE_OK;
}
