#include "../../include/image/image.h"
#include <stdint.h>
#include <stdlib.h>

struct image create(struct image* image , uint64_t width, uint64_t height){
    image->width = width;
    image->height = height;

    image->data = (struct pixel*) malloc(width * height * sizeof(struct pixel));
    if (!image->data) {
        free(image);
        return (struct image){.width = 0, .height = 0, .data = NULL};;
    }

    return *image;
}

uint64_t calculate_padding(uint64_t width){
    const uint64_t PIXEL_SIZE = sizeof(struct pixel);
    uint64_t bytes = width * PIXEL_SIZE;
    uint64_t remainder = bytes % 4;

    uint64_t padding = (4 - remainder) % 4;
    return padding;
}


void free_memory(struct image* image){
    if (image){
        if(image->data){
            free(image->data);
        }
        free(image);
    }
}
